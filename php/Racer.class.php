<?php

	class Racer{

		protected $stream;
		protected $race;
		protected $data;
		
		public function __construct(\Connection $connection){
			$this->stream = new StreamConnection($connection);
			$this->race 	= new Race;
			WHILE($this->stream->isAlive()): 
				$this->data = $this->stream->getData();
				$this->drive();
			ENDWHILE;
		}

		protected function drive(){
			SWITCH($this->data['msgType']):
				case 'carPositions':
					$this->race->car->setPosition($this->data);
					$this->stream->send('throttle', 0.6);
					#$this->stream->send('switchLane', 'Right');
					break;
				case 'join':
					$this->stream->ping();
					break;
				case 'yourCar':
					$this->race->car->paint($this->data['data']['color']);
					$this->stream->ping();
					break;
				case 'gameInit':
					$this->race->car->registerForRace($this->race);
					$this->stream->ping();
					break;
				case 'gameStart':
					$this->stream->send('switchLane', 'Right');
					break;
				case 'crash':
					$this->stream->ping();
					break;
				case 'spawn':
					$this->stream->ping();
					break;
				case 'lapFinished':
					exit;
				case 'dnf':
					$this->stream->ping();
					break;
				case 'finish':
					exit;
				default:
					$this->stream->ping();
			ENDSWITCH;
		}

	}

?>

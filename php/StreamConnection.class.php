<?php
define('MAX_LINE_LENGTH', 1024 * 1024);

class StreamConnection{

	protected $connection;
	protected $data;

	function __construct(\Connection $connection){
		$this->connection = $connection;
		IF(!$this->connect()):
			$this->connection = new Connection;
			$this->connect();
		ENDIF;
		$this->send('join', array(
			'name' => $this->connection->botname,
			'key'	 => $this->connection->botkey
		));
	}

	function __destruct(){
		IF(isset($this->connection->sock)):
			socket_close($this->connection->sock);
		ENDIF;
	}

	protected function connect(){
		$this->connection->sock = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		return ($this->connection->sock === false || @!socket_connect($this->connection->sock, $this->connection->host, $this->connection->port))
			? false
			: true;
	}

	protected function read_msg(){
		$line = @socket_read($this->connection->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		$line === false
			? $this->debug('** ' . $this->sockerror())
			: $this->debug('<= ' . rtrim($line));
		return json_decode($line, true);
	}

	public function send($msgtype, $data){
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
		$this->debug('=> ' . rtrim($str));
		IF(@socket_write($this->connection->sock, $str) === false):
			throw new Exception('write: ' . $this->sockerror());
		ENDIF;
	}
	
	protected function sockerror(){
		return socket_strerror(socket_last_error($this->connection->sock));
	}
	
	protected function debug($msg){
		IF($this->connection->debug):
			echo $msg, "\n";
		ENDIF;
	}
	
	public function isAlive(){
		IF(!is_null($this->data = $this->read_msg())):
			return true;
		ENDIF;
	}

	public function getData(){
		return $this->data;
	}

	public function ping(){
		$this->send('ping', null);
	}

}
?>

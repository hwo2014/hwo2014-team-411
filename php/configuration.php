<?php

	/* Requires */
	require_once('StreamConnection.class.php');
	require_once('Struct.class.php');
	require_once('Racer.class.php');
	require_once('Race.class.php');
	require_once('Error.class.php');
	require_once('Car.class.php');
	require_once('Misc.class.php');

	/* Connection Settings */
	const HOST_NAME						= "testserver.helloworldopen.com";
	const HOST_PORT						= 8091;
	const BOT_NAME						= "RUNFORESTRUN";
	const BOT_KEY							= "xUVFSTw/pD3I3Q";
	const LOCAL_DEBUG					= false;

	/* Tuning */
	const STANDARD_THROTTLE		= 0.6;
	const STANDARD_POWER			= 1.0;

	/* "Structs" */
	class Connection extends Struct{
		public $host		= HOST_NAME;
		public $port		= HOST_PORT;
		public $botname	= BOT_NAME;
		public $botkey 	= BOT_KEY;
		public $debug		= LOCAL_DEBUG;
		public $sock;
	}

	class Position extends Struct{
		public $angle;
		public $pieceindex;
		public $localdistance;
		public $lane;
		public $speed;
		public $offtrack = false;
	}

	class LapEvents extends Struct{
		public $hasbeenofftrack = false;
	}

	class Engine extends Struct{
		public $throttle	= STANDARD_THROTTLE;
		public $power			= STANDARD_POWER;
	}

	class Speed extends Struct{
		public $lastdistance		= 0;
		public $currentspeed		= 0;
		public function __toString(){
			return $this->currentspeed;
		}		
	}

?>

<?php

	class Car extends Struct{

		public $position;
		public $engine;
		public $color;
		public $length;
		public $width;
		public $race;

		public function setPosition($data){
			$this->position->angle					= $this->get($data, "angle");
			$this->position->pieceindex			= $this->get($data, "pieceIndex");
			$this->position->localdistance	= $this->get($data, "inPieceDistance");
			#$this->position->speed					= ($this->position->localdistance - $this->position->speed->lastdistance) *-1
		}

		public function registerForRace(&$race){
			$this->race = $race;
		}

		public function paint($color){
			$this->color = error::onNullValue($color);
		}

		public function get($data, $get){
			FOREACH($data as $assoc => $val):
				IF(isset($assoc["id"]["color"])):
					$this->currentcolor = $assoc["id"]["color"];
				ENDIF;
				IF($assoc === $get && @$this->currentcolor === $this->color):
					$this->getreturn = $val;
					break;
				ENDIF; 
				IF(is_array($val)): 
					$this->get($val, $get);
				ENDIF;
			ENDFOREACH;
			return @$this->getreturn;
		}

	}

?>

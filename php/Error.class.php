<?php

	class error{

		public static function nullValue(){
			echo "Stopped execution on null value";
			exit;
		}

		public static function onNullValue(){
			FOREACH(func_get_args() as $arg):
				IF(empty($arg)):
					error::nullValue();
				ENDIF;
			ENDFOREACH;
		}

	}

?>
